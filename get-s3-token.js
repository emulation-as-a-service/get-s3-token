#!/usr/bin/env node

import process from "process";
import STS from "aws-sdk/clients/sts.js";
import * as assert from "assert";

const {
  AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY,
  AWS_DEFAULT_REGION,
  EAAS_S3_BUCKET_URL,
} = process.env;

const [username] = process.argv.slice(2);

assert.ok(typeof username === "string", "username must be present");

const bucket = "user-data";
const Resource = `arn:aws:s3:::${bucket}`;

const directory = `${username}`;
assert.doesNotMatch(directory, /\//, "must not contain subdirectories");
assert.match(
  directory,
  /^[_a-zA-Z0-9+=:-]+$/,
  "must not contain special characters"
);

// See https://aws.amazon.com/blogs/security/writing-iam-policies-grant-access-to-user-specific-folders-in-an-amazon-s3-bucket/
// Actions, Conditions: https://docs.aws.amazon.com/AmazonS3/latest/dev/list_amazons3.html
// Condition Operators: https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_condition_operators.html

const statements = [
  {
    Sid: "AllowListingOfUserFolder",
    Action: ["s3:ListBucket"],
    Effect: "Allow",
    Resource: [Resource],
    Condition: { StringLike: { "s3:prefix": [`${directory}/*`] } },
  },
  {
    Sid: "AllowGetPutDeleteInUserFolder",
    Effect: "Allow",
    Action: ["s3:GetObject", "s3:PutObject", "s3:DeleteObject"],
    Resource: [`${Resource}/${directory}/*`],
  },
];

(async () => {
  const sts = new STS({
    endpoint: EAAS_S3_BUCKET_URL,
    s3BucketEndpoint: true,
    signatureVersion: "v4",
    region: AWS_DEFAULT_REGION,
    credentials: {
      accessKeyId: AWS_ACCESS_KEY_ID,
      secretAccessKey: AWS_SECRET_ACCESS_KEY,
    },
  });

  const { Credentials } = await sts
    .assumeRole({
      DurationSeconds: 3600,
      RoleArn: "arn:xxx:xxx:xxx:xxxx",
      RoleSessionName: "eaas-get-s3-token-mount",
      Policy: JSON.stringify({
        Version: "2012-10-17",
        Statement: statements,
      }),
    })
    .promise();

  const rootUrl = new URL(EAAS_S3_BUCKET_URL);
  rootUrl.pathname = "/";

  const ret = {
    type: "s3",
    endpoint: String(rootUrl),
    region: AWS_DEFAULT_REGION,
    credentials: Credentials,
    path: `${bucket}/${directory}`,
    prefix: `${directory}/`,
  };

  console.log(JSON.stringify(ret));
})().catch(err => { console.error(err); process.exit(1); });
