#!/usr/bin/env node

import process from "process";
import S3 from "aws-sdk/clients/s3.js";
import * as assert from "assert";

const {
  AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY,
  AWS_DEFAULT_REGION,
  EAAS_S3_BUCKET_URL,
} = process.env;

const bucket = "user-data";

(async () => {
  const s3 = new S3({
    endpoint: EAAS_S3_BUCKET_URL,
    s3BucketEndpoint: true,
    signatureVersion: "v4",
    region: AWS_DEFAULT_REGION,
    credentials: {
      accessKeyId: AWS_ACCESS_KEY_ID,
      secretAccessKey: AWS_SECRET_ACCESS_KEY,
    },
    logger: console,
  });

  console.log(
    await s3.createBucket({ Bucket: bucket, ACL: "private" }).promise()
  );

  // console.log(JSON.stringify(ret));
})().catch(console.error);
